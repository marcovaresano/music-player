let songs = [
    {  url : 'songs/026 - Nirvana - Smells Like Teen Spirit.mp3', cover : 'https://cdns-images.dzcdn.net/images/cover/f0282817b697279e56df13909962a54a/500x500.jpg', artist : "Nirvana", title : 'Smell Teen Like A Spirit', durata: '5.01'},
    {  url : './songs/002 - Van Halen - Eruption.mp3', cover : 'https://images-na.ssl-images-amazon.com/images/I/71wSKT-YLPL._AC_SX425_.jpg', artist : "Van Halen", title : 'Eruption', durata: '1.42'},
    {  url : './songs/003 - Lynyrd Skynyrd - Free Bird.mp3', cover : 'https://images-na.ssl-images-amazon.com/images/I/714INNxTsvL._AC_SX355_.jpg', artist : "Lynyrd Skynyrd", title : 'Free Bird', durata: '9.07'},
    {  url : './songs/004 - Pink Floyd - Comfortably Numb.mp3', cover : 'https://iv1.lisimg.com/image/15004314/500full.jpg', artist : "Pink Floyd", title : 'Comfortably Numb', durata: '6.24'},
    {  url : './songs/005 - Jimi Hendrix Experience - All Along The Watchtower.mp3', cover : 'https://img.discogs.com/3GgBe5cyX4RIPBX-a9kTOuHhqwk=/fit-in/351x350/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-4261514-1360025582-7984.jpeg.jpg', artist : "Jimi Hendrix Experience", title : 'All Along The Watchtower', durata: '4.00'},
    {  url : 'songs/alarm.wav', cover : 'https://img.discogs.com/3GgBe5cyX4RIPBX-a9kTOuHhqwk=/fit-in/351x350/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-4261514-1360025582-7984.jpeg.jpg', artist : "Jimi Hendrix Experience", title : 'All Along The Watchtower', durata: '4.00'},

]





// traccia audio
const track = document.querySelector('#track')
//action buttons
const playBtn = document.querySelector('.btn-play')
const pauseBtn = document.querySelector('.btn-pause')
const cover = document.querySelector('.album-cover')
const hole = document.querySelector('.hole')
const vinyl = document.querySelector('.vinyl')
const btnsSidebar = document.querySelectorAll('.btn-c')
let playing = false;
const playlistWrapper = document.querySelector('#playlist-wrapper')
let currentTrack = 0;
const trackTitle = document.querySelector('#track-title')
const trackArtist = document.querySelector('#track-artist')
const btnNextTrack = document.querySelector('#btnNextTrack')
const btnPrevTrack = document.querySelector('#btnPrevTrack')
const btnShuffle = document.querySelector('#shuffle')
const btnVolx = document.querySelector('.btn-volx')
const btnVol = document.querySelector('.btn-vol')
const btnPlaySongList = document.querySelector('.play-btn-list-cover')
const btnPauseSongList = document.querySelector('.pause-btn-list-cover')
const startSec = document.querySelector('#startSec')
const finishSec = document.querySelector('#finishSec')
const progBar = document.querySelector('#progress-bar')


function play() {
    playBtn.classList.toggle('d-none')
    pauseBtn.classList.toggle('d-none')
    cover.classList.toggle('play')
    hole.classList.toggle('d-none')
    track.play() 
    playing = true;
}


function pause() {
playBtn.classList.toggle('d-none')
pauseBtn.classList.toggle('d-none')
cover.classList.toggle('play')
hole.classList.toggle('d-none')
track.pause() 
playing = false;
}




playBtn.addEventListener('click', () => {
    play()
})

pauseBtn.addEventListener('click', () => {
    pause()
})


btnsSidebar.forEach((btn) =>{
    btn.addEventListener('click', ()=> {
        let selectedMenu = btn.getAttribute('btn-menu');
        let menu = document.querySelector(`.menu-wrapper-${selectedMenu}`)

        let sections = document.querySelectorAll('.menu-wrapper')
        sections.forEach((section)=> {
            section.classList.add('d-none')
        })

        btnsSidebar.forEach((btn) => {
            btn.classList.remove('active')
        })

        btn.classList.add('active')

        menu.classList.add('d-block')
        menu.classList.remove('d-none')
    })
})


function populateSongList(){
    songs.forEach((track, index) => {
        let card = document.createElement('div')
        
        card.innerHTML = ` 
        <div class="row my-3 align-items-center">
        <div class="col-4 col-md-2 pb-3">
        <div class="position-relative">
            <img class="img-fluid song-list-cover" src="${track.cover}" alt="" width="100px" height="auto">
        </div>        
    </div>
    <div class="col-4 col-md-5 text-start">
        <h6 class="track-list-info">${track.title} - ${track.artist}</h6>
    </div>
    <div class="col-3 col-md-2 d-flex justify-content-evenly align-items-center">
       <span class="track-durata">${track.durata}</span>
    </div>

    <div class="col-4 col-md-2 d-flex justify-content-evenly align-items-center">
        <button class="play-btn-list-cover" track-index="${index}"><i class="fa-solid fa-play fs-3 text-white"></i></button>
        <button class="btn pause-btn-list-cover d-none" track-index="${index}"><i class="fa-solid fa-pause fs-3 text-white"></i></button>
    </div>
    </div>
    `

        playlistWrapper.appendChild(card)
    })
}


function changeTrackDetails() {
    trackArtist.innerHTML = songs[currentTrack].artist
    trackTitle.innerHTML = songs[currentTrack].title
    cover.src = songs[currentTrack].cover
    track.src = songs[currentTrack].url
}

 function nextTrack() {
     currentTrack ++
     if (currentTrack > songs.length - 1) {
        currentTrack = 0
    }
     changeTrackDetails()
     if(playing) {
        pause();
        play();
    } 
}






 function prevTrack() {
    currentTrack --
    if (currentTrack < 0) {
        currentTrack = songs.length -1
        console.log(currentTrack)
    }
    changeTrackDetails()

    if(playing) {
        pause();
        play();
  }
}





 btnNextTrack.addEventListener('click', () => {
     nextTrack()
     songDurate()
     
 })

 btnPrevTrack.addEventListener('click', () => {
    prevTrack()
    songDurate()
    
})


function songDurate() {
        finishSec.innerHTML = `${(track.duration * 0.0167).toFixed(2).replace('.',':')}`
        startSec.innerHTML = `${(track.currentTime * 0.0167).toFixed(2).replace('.',':')}`
}

track.addEventListener ("timeupdate",  () => {
    let currentTime = track.currentTime;
    let duration = track.duration
    songDurate()
    
    let advancingTime = (currentTime + .25 ) /duration*100;
    progBar.value = `${advancingTime}`
    if (progBar.value == 100) {
        nextTrack()
    }

})


function updateProgressBar() {
  const percentage = Math.floor((100 / track.duration) * track.currentTime);
  progBar.value = percentage;
  progBar.innerHTML = percentage + '% played';
}



$("#progress-bar").on("click", function(e) {
    updateProgressBar()
    var progbar = $('#progress-bar');
    var pos = e.pageX - progbar.offset().left; //Position cursor
    var percent = pos / progbar.width(); //Get width element
    track.currentTime = percent * track.duration;
    progbar.value = percent / 100;
    console.log(progbar.value)

    });
    




function changeSong(index) {
    currentTrack = index
    changeTrackDetails()
    pause();
    play();
}



function shuffle() {
    currentTrack = Math.floor(Math.random() * (songs.length));
    changeTrackDetails()
    if(playing) {
        pause()
        play();
  }
}



btnShuffle.addEventListener('click', () => {
    shuffle()
})


// btnPlaySongList.addEventListener('click'. () => {
//     changeSong()
// })

function attachChangeSongEvent() {
    let btns = document.querySelectorAll('.play-btn-list-cover');
    btns.forEach(btn => {
        btn.addEventListener('click', () => {
            let index = btn.getAttribute('track-index')
            changeSong(index)
        })
    })
   
}


btnVolx.addEventListener('click', () => {
    if (track.volume == 1) {
        track.volume = 0 
        btnVolx.classList.add('activevol')
     } else {
            track.volume = 1
            btnVolx.classList.remove('activevol')
        }
    console.log(track.volume)
})



btnVol.addEventListener('click', () => {
    let volWrapper = document.querySelector('.volume-wrapper')
    volWrapper.classList.toggle('d-none')
    btnVol.classList.toggle('active')
    
})


let volume = document.querySelector("#volume-control");
volume.addEventListener("change", function(e) {
track.volume = e.currentTarget.value / 100;
})


// function volumeBar () {
//     let volumeBar = document.createElement('input')

//     volumeBar.innerHTML =  `<input type="range" id="volume-control"> `
//     document.appendChild('.volume-wrapper')
// }






// avvio delle funzioni
populateSongList()

attachChangeSongEvent()







